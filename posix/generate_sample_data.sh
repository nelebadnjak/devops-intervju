#!/bin/bash


for a in {1..30};
do
  mkdir dir$a/;
  cd dir$a;

  for i in {1..10};
  do
    dd if=/dev/urandom of=project_resurs$((1 + $RANDOM % 10000))_dev.yaml bs=10K count=1;
  done;
  cd ..;

done
