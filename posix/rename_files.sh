#!/bin/bash

for f in $(find . | grep project_resurs | grep dev.yaml);
do

	indetifer=$(basename $f | sed -e 's/project_resurs\(.*\)_dev.yaml/\1/')
	mv $f $(dirname $f)/project_resurs${indetifer}_prod.yaml;
	echo "$f,$(dirname $f)/project_resurs${indetifer}_prod.yaml";

done
