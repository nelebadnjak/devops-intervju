#!/bin/bash
set -e

printenv;
echo "Creating user";
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    create user ${PROJECT_USER} with encrypted password '${PROJECT_PASS}';
    CREATE DATABASE ${PROJECT_DB};
    GRANT ALL PRIVILEGES ON DATABASE ${PROJECT_DB} TO ${PROJECT_USER};
EOSQL