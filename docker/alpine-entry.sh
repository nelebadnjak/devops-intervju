#!/bin/sh

echo "Starting client";

until pg_isready -h project_db_server -p 5432 -U $POSTGRES_USER
do
  echo "Waiting for postgres"
  sleep 2;
done

export PGPASSWORD=$PROJECT_PASS;

psql -v ON_ERROR_STOP=1 --username "$PROJECT_USER" --dbname "$PROJECT_DB" --host project_db_server <<-EOSQL
	CREATE TABLE IF NOT EXISTS ${PROJECT_TABLE} (
	  id serial PRIMARY KEY,
	  start_date timestamp default NULL
	)
EOSQL

psql -v ON_ERROR_STOP=1 --username "$PROJECT_USER" --dbname "$PROJECT_DB" --host project_db_server <<-EOSQL
	INSERT into ${PROJECT_TABLE} (start_date) 
	VALUES (current_timestamp);
EOSQL

echo "DONE!!!";
exec "$@"

