#!/bin/bash

if [ ! command -v docker &> /dev/null];
then
    echo "Docker nije instaliran, pogledaj: https://get.docker.com/"
    exit
fi

if [ ! command -v docker-compose &> /dev/null];
then
    echo "docker-compose nije instaliran, pogledaj: https://docs.docker.com/compose/install/"
    exit
fi

if [ ! -f database.env]; 
then
    echo "Fajl sa kredencijalima ne postoji, generisem jedan"
    echo """
POSTGRES_DB=postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=project_pwd

PROJECT_USER=project_user
PROJECT_PASS=project_pass
PROJECT_TABLE=project_table
PROJECT_DB=project_db
""" > database.env
	cat database.env;
fi

echo "Pokrecem docker-compose";
sleep 3;
docker-compose up;